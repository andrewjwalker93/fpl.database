IF NOT EXISTS (
		SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_NAME = 'PER_PERSONS')
BEGIN

	CREATE TABLE PER_PERSONS
	(
		PER_Key int identity(1,1),
		PER_Name nvarchar(50) not null,

		CONSTRAINT PK_PER_KEY PRIMARY KEY (PER_Key)
	)

	INSERT INTO PER_PERSONS(PER_Name)
		VALUES
			('Josh'),
			('Andrew'),
			('Will'),
			('Rob'),
			('Jack'),
			('Luke'),
			('Nik')
END
