IF NOT EXISTS (
		SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_NAME = 'FLE_Fantasy_League_Event')
BEGIN

	CREATE TABLE FLE_Fantasy_League_Event
	(
		FLE_Key int identity(1,1),
		FLE_Player int not null,
		FLE_Amount int not null,
		FLE_GameID int not null,
		FLE_Event_Type int not null,

		CONSTRAINT PK_FLE_KEY PRIMARY KEY (FLE_Key),
		CONSTRAINT FK_FLE_Event_Type FOREIGN KEY (FLE_Event_Type) REFERENCES NT_NOTIFICATION_TYPES([NT_KEY]),
	)
END
