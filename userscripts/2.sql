IF NOT EXISTS (
		SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_NAME in ('FT_Fantasy_Titles', 'FTT_Fantasy_Title_Types'))
BEGIN

	CREATE TABLE FTT_FANTASY_TITLE_TYPES
	(
		FTT_Key int identity(1,1),
		FTT_Name nvarchar(50) not null,

		CONSTRAINT PK_FTT_KEY PRIMARY KEY (FTT_Key)
	)

	CREATE TABLE FT_FANTASY_TITLES
	(
		FT_Key int identity(1,1),
		FT_FTT_KEY int not null,
		FT_YEAR smallint not null,
		FT_WINNING_PERSON int not null,

		CONSTRAINT PK_FT_KEY PRIMARY KEY (FT_Key),
		CONSTRAINT FK_FT_FTT_KEY FOREIGN KEY (FT_FTT_KEY) REFERENCES FTT_FANTASY_TITLE_TYPES([FTT_KEY]),
		CONSTRAINT FK_FT_WINNING_PERSON FOREIGN KEY (FT_WINNING_PERSON) REFERENCES PER_PERSONS([PER_KEY])
	)

	INSERT INTO FTT_FANTASY_TITLE_TYPES(FTT_Name)
		VALUES
			('FPL'),
			('EURO'),
			('WORLD CUP')

	INSERT INTO FT_FANTASY_TITLES(FT_FTT_KEY, FT_YEAR, FT_WINNING_PERSON)
	VALUES
		(2, 2012, 5),
		(1, 2012, 1),
		(1, 2013, 5),
		(3, 2014, 7),
		(1, 2014, 1),
		(1, 2015, 3),
		(2, 2016, 1),
		(1, 2016, 3),
		(1, 2017, 1),
		(3, 2018, 4),
		(1, 2018, 3),
		(1, 2019, 1),
		(1, 2020, 1),
		(2, 2020, 7)
END